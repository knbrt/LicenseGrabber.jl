using Test


using LicenseGrabber

@testset "Testing the tests" begin
    # go to actual environment lever
    cd(dirname(Base.active_project()))

    # check license dictionary
    ld = LicenseGrabber.getlicloc()
    @test haskey(ld, "JULIA")

    # check whole run
    LicenseGrabber.grab!("Project.toml")
    @test isdir(".license_information")
end # test
