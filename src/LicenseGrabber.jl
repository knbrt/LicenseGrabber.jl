"""
    LicenseGrabber

# Description
LicenseGrabber provides functionalities to gather Information regarding the current active Julia Environment and the
licenses used by it's dependencies.
"""
module LicenseGrabber

import Pkg



"""
    getlicloc()
    getlicloc(envpath::T; ignorepaths=[".license_information"]) where T <: AbstractString

# Description
getlicloc gathers all license file location of all (direct and indirect)
licenses file of the active julia environment.

# Example
```
julia> LicenseGrabber.getlicloc()
  Activating environment at `~/Code/LicenseGrabber/Project.toml`
  Activating environment at `~/Code/LicenseGrabber/Project.toml`
Dict{String, Vector{AbstractString}} with 1 entry:
  "JULIA" => ["/home/knbrt/Tools/julia/1.6.0/LICENSE.md"]
```

# Meta
## Uses
## Used by
- [`grab!`](@ref)
"""
function getlicloc(envpath::T; ignorepaths=[".license_information"]) where T <: AbstractString
    # cache active project
    activeproject = Base.active_project()

    # input sanity & activation of 
    let p1 = envpath, p2 = joinpath(envpath, "Project.toml")
        isfile(p1) && basename(p1) == "Project.toml" && begin
            Pkg.activate(p1)
        end # begin
        isfile(p2) && begin 
            Pkg.activate(p2)
        end # begin
    end # let

    # get dependencies and prepare return
    deps = Pkg.dependencies()
    licenseDict = Dict{String, Array{AbstractString,1}}()

    # regex to searcho for
    r = r"(?i)license(.md)?$"

    for (uuid, dep) in deps
        # only non stdlib packages are interesting
        dep.version === nothing && begin
            stddir = joinpath(splitpath(dep.source)[1:end-5]...)
            licenseDict["JULIA"] = [joinpath(stddir, "LICENSE.md")]
            continue
        end # begin

        licenseDict[dep.name] = Array{AbstractString,1}()
        # search in source files of dep for license
        for (root, dirs, files) in walkdir(dep.source)
            any([x ⊆ root for x in ignorepaths]) && continue
            catchindex = map(x -> isa(match(r, x), RegexMatch), files)
            catches = findall(x -> x ≠ 0, catchindex)
            # push licenses to dict
            push!(licenseDict[dep.name], map(x->joinpath(root, files[x]), catches)...)
        end # for
    end # for

    # clean up & return to origin environment
    Pkg.activate(activeproject)
    return licenseDict
end # function

getlicloc() = getlicloc(".")



"""
    cplicenses!(ld::Dict{String, AbstractString}, destination::AbstractString)

# Description
cplicenses! copies the license files specified in ld to `destination`.

# Meta
## Uses
## Used by
- [`grab!`](@ref)
"""
function cplicenses!(ld::Dict{String, Array{AbstractString, 1}}, path::AbstractString)
    # empty the dir
    isdir(path) && begin
        for (root, dirs, files) in walkdir(path)
            map(x->rm(joinpath(root,x)), files)
        end # for
    end # begin

    # create path
    isdir(path) || mkpath(path)
    for (k,v) in ld
        for f in v
            # identify stdlib
            k == "JULIA" && begin
                destination = joinpath(path, k, basename(f))
            end
            k ≠ "JULIA" && begin
                r = Regex("$(k)(.jl)?")
                i = findall(x->isa(match(r, x), RegexMatch), splitpath(f))[1]
                destination = joinpath(path, k, splitpath(f)[i+2:end]...)
            end
            targetdir = dirname(destination)
            isdir(targetdir) || mkpath(targetdir)
            cp(f, destination, force=true)
        end # for
    end # for
end # function



"""
    parselicenses(path::AbstractString)

# Description
parselicenses parses all license file in `path` oder `standard destination`
for known license types.

This is under development and not stable.  

# Meta
## Uses
## Used by
- [`grab!`](@ref)
"""
function parselicenses(path::AbstractString)
    # input sanity
    isdir(path) || throw(ErrorException("$(path) ≡ Bad. Must provide a dir"))

    # loop through path and search in dir for known licenses.
    for (root, dir, files) in walkdir(path)
        for file in files
            file = open(f->read(f,String), joinpath(root,file))
            r = match(r"MIT", file)
            occursin("MIT", file) && println(r.match)
            occursin("MIT", file) || println("no")
        end # for
    end # for
end # function



"""
    grab!()
    grab!(projectfile::String)

# Description
grab! grabs all licenses specified in `projectfile` or in the current `active
environment`, copies them to the default location (see [`cplicenses!`](@ref))
and parses them for the known license types.

# Meta
## Usage
- API
## Uses
- [`getlicloc`](@ref)
- [`cplicenses!`](@ref)
- Soon: [`parselicenses`](@ref)

## Used by
## FutureFeature
Will be used by a license parsing function which compares license files to
a database of existing license files to identify the used license. See
[Milestone v0.1.0](https://gitlab.com/knbrt/LicenseGrabber.jl/-/milestones/2)
"""
function grab!(projectfile::String)
    # read project file if existing and propper name
    isfile(projectfile) || throw(ErrorException("$projectfile ≡ BAD."))
    basename(projectfile) ≠ "Project.toml" && begin
        throw(ErrorException("$projectfile ≡ BAD."))
    end # begin

    @info "get license locations"
    source_dir = getlicloc(projectfile)

    # get license information location
    target_loc = joinpath(dirname(projectfile), ".license_information")
    # create run instance
    isdir(target_loc) || mkpath(target_loc)

    @info "copy license files"
    cplicenses!(source_dir, target_loc)
    #@info "parse license files"
    #parselicenses(target_run_loc)
end # function

grab!() = grab!(Base.active_project())



end # module
