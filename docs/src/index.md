# Documentation

```@contents
```

## Installation
You LicenseGrabber.jl like most Julia packages

```
julia>] add LicenseGrabber
```

## Overview
License Grabber consists of one single module.
```@docs
LicenseGrabber
```

## Example
Let us start with this very repository as an example. Here we have the following folder structure:
```
>>> tree -ra . -I '.git'
.
├── test
│   ├── runtests.jl
│   ├── Project.toml
│   └── Manifest.toml
├── src
│   └── LicenseGrabber.jl
├── README.md
├── Project.toml
├── Manifest.toml
├── LICENSE.md
├── .gitlab-ci.yml
├── .gitignore
└── docs
    ├── src
    │   └── index.md
    ├── Project.toml
    ├── Manifest.toml
    └── make.jl

4 directories, 14 files
```

Since there is only one dependency: `Julia` itself, let us add some packages:
```
julia>]add GLM
```
After it's installation, let us grab the now used licences in this repo (excluding it's own license...)
```
julia> using LicenseGrabber
julia> LicenseGrabber.grab!()
[ Info: get license locations
  Activating environment at `~/Code/LicenseGrabber/Project.toml`
  Activating environment at `~/Code/LicenseGrabber/Project.toml`
[ Info: copy license files
```

As the result we obtain the following folderstructure
```
>>> tree -ra . -I '.git'
.
├── test
│   ├── runtests.jl
│   ├── Project.toml
│   └── Manifest.toml
├── src
│   └── LicenseGrabber.jl
├── README.md
├── Project.toml
├── Manifest.toml
├── LICENSE.md
├── .license_information
│   ├── TableTraits
│   │   └── LICENSE.md
│   ├── Tables
│   │   └── LICENSE
│   ├── StructTypes
│   ├── StatsModels
│   │   └── LICENSE.md
│   ├── StatsFuns
│   │   └── LICENSE.md
│   ├── StatsBase
│   │   └── LICENSE.md
│   ├── SpecialFunctions
│   │   └── LICENSE
│   ├── SortingAlgorithms
│   │   └── LICENSE.md
│   ├── ShiftedArrays
│   │   └── LICENSE.md
│   ├── Rmath_jll
│   │   └── LICENSE
│   ├── Rmath
│   │   └── LICENSE.md
│   ├── Reexport
│   │   └── LICENSE.md
│   ├── QuadGK
│   │   └── LICENSE
│   ├── PrettyTables
│   ├── Preferences
│   │   └── LICENSE.md
│   ├── PooledArrays
│   ├── PDMats
│   │   └── LICENSE.md
│   ├── Parsers
│   ├── OrderedCollections
│   │   └── License.md
│   ├── OpenSpecFun_jll
│   │   └── LICENSE
│   ├── Missings
│   │   └── LICENSE.md
│   ├── JULIA
│   │   └── LICENSE.md
│   ├── JSON
│   ├── JLLWrappers
│   │   ├── test
│   │   │   ├── Vulkan_Headers_jll
│   │   │   │   └── LICENSE
│   │   │   ├── OpenLibm_jll
│   │   │   │   └── LICENSE
│   │   │   └── HelloWorldC_jll
│   │   │       └── LICENSE
│   │   └── LICENSE
│   ├── IteratorInterfaceExtensions
│   │   └── LICENSE.md
│   ├── InvertedIndices
│   ├── GLM
│   │   └── LICENSE.md
│   ├── Formatting
│   ├── FillArrays
│   │   └── LICENSE
│   ├── Distributions
│   │   └── LICENSE.md
│   ├── DataValueInterfaces
│   │   └── LICENSE.md
│   ├── DataStructures
│   │   └── License.md
│   ├── DataFrames
│   ├── DataAPI
│   │   └── LICENSE.md
│   ├── Crayons
│   ├── Compat
│   │   └── LICENSE.md
│   ├── ChainRulesCore
│   │   └── LICENSE.md
│   └── CategoricalArrays
├── .gitlab-ci.yml
├── .gitignore
└── docs
    ├── src
    │   └── index.md
    ├── Project.toml
    ├── Manifest.toml
    └── make.jl

47 directories, 45 files
```
including a copy of every License file found in a dependency.


## Functions
```@docs
LicenseGrabber.getlicloc
```

```@docs
LicenseGrabber.cplicenses!
```

```@docs
LicenseGrabber.parselicenses
```

```@docs
LicenseGrabber.grab!
```

## Index
```@index
```
