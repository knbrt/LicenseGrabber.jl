# LicenseGrabber.jl
LicenseGrabber.jl aims to be a simple local package / tool to help with all the licenses of julia packages used in your own project.

See [docs](https://knbrt.gitlab.io/LicenseGrabber.jl/)

So far LicenseGrabber is still young and immature.

# Usage
## Installation
```
julia>] add LicenseGrabber
```

## Application
```
julia> using LicenseGrabber
julia> LicenseGrabber.grab!()
```
